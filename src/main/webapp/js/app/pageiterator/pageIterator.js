/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.PageIterator = function(array) {

    /* ************************************************************
     * Initialization.
     *************************************************************/
    var pos;

    //Always return a new instances, even if called without 'new'
    if (!(this instanceof unoreader.PageIterator)) {
        return new unoreader.PageIterator(array);
    }

    init();

    /** Initialization actions for this instance. */
    function init() {
        pos = 0;
    }


    /* ************************************************************
     * Public functions.
     *************************************************************/
    function next(pageSize) {
        var result;
        if (pos < array.length) {
            if (pos + pageSize <= array.length) {
                result = array.slice(pos, pos + pageSize);
                pos = pos + pageSize;
            }
            else {
                result = array.slice(pos, array.length);
                pos = array.length;
            }
        }
        else {
            result = [];
        }
        return result;
    }

    function hasNext() {
        return array.length === pos? false : true;
    }


    /* ************************************************************
     * Public object attributes.
     *************************************************************/

    this.next = next;
    this.hasNext = hasNext;

};