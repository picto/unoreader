/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service.impl;

import com.ideasagiles.reader.domain.Feed;
import com.ideasagiles.reader.domain.FeedPack;
import com.ideasagiles.reader.repository.FeedPackRepository;
import com.ideasagiles.reader.service.FeedPackService;
import com.ideasagiles.reader.service.FeedService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FeedPackServiceImpl implements FeedPackService {

    @Autowired
    private FeedService feedService;
    @Autowired
    private FeedPackRepository feedPackRepository;

    @PreAuthorize("isAuthenticated()")
    @Override
    public void addPackToUser(String packName) {
        List<FeedPack> feedPacks = feedPackRepository.findByPack(packName);
        for (FeedPack pack : feedPacks) {
            Feed feed = new Feed();
            feed.setChannel(pack.getChannel());
            feed.setName(pack.getName());
            feed.setUrl(pack.getUrl());
            try {
                feedService.save(feed);
            }
            catch (DataIntegrityViolationException ex) {
                //ignore duplicates and continue
            }
        }
    }

}
