/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.domain.Favourite;
import java.util.List;

/**
 * Service class for Favourite entries.
 */
public interface FavouriteService {

    List<Favourite> findAll();
    void save(Favourite favourite);
    void delete(Long id);

}
