/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.security;

import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.UserRepository;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A handler that executes when an authentication is successful.
 * This handler saves the last login time for the user, when it authenticates
 * via the login form, or using the remember-me cookie.
 */
@Service("security.AuthenticationSuccessEventImpl")
@Transactional
public class AuthenticationSuccessEventImpl extends SavedRequestAwareAuthenticationSuccessHandler implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    private UserRepository userRepository;

    /**
     *  Event that registers logins via the remember-me cookie.
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws ServletException, IOException {
        String username = ((UserDetails) auth.getPrincipal()).getUsername();
        updateLastLogin(username);
        super.onAuthenticationSuccess(request, response, auth);
    }

    /**
     * Event that registers regular logins (via form).
     */
    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        String username = ((UserDetails) event.getAuthentication().getPrincipal()).getUsername();
        updateLastLogin(username);
    }

    private void updateLastLogin(String username) {
        User user = userRepository.getByUsername(username);
        user.setLastLogin(new Date());
    }
}
