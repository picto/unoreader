/*
 * Schema creation script for in-memory database.
 */
drop table if exists feed_pack;
drop table if exists feed;
drop table if exists authority;
drop table if exists favourite;
drop table if exists password_recovery;
drop table if exists user;

create table user (
    id bigint identity primary key,
    username varchar(50) unique not null,
    password varchar(100) not null,
    enabled boolean not null,
    member_since datetime not null,
    last_login datetime
);

create table authority (
    id bigint identity primary key,
    id_user bigint not null,
    authority varchar(50) not null,
    constraint fk_authorities_user foreign key(id_user) references user(id)
);
create unique index ix_auth_user on authority(id_user,authority);

create table feed (
    id bigint identity primary key,
    name varchar(100) not null,
    url varchar(500) not null,
    channel varchar(50) not null,
    created_on datetime not null,
    id_user bigint not null,
    constraint fk_feed_user foreign key(id_user) references user(id)
);
create unique index ix_url_channel_user on feed(url,channel,id_user);

create table feed_pack (
    id bigint identity primary key,
    pack varchar(50) not null,
    name varchar(100) not null,
    url varchar(500) not null,
    channel varchar(50) not null
);

create table favourite (
    id bigint identity primary key,
    title varchar(200) not null,
    snippet varchar(1000),
    url varchar(500) not null,
    created_on datetime not null,
    id_user bigint not null,
    constraint fk_favourite_user foreign key(id_user) references user(id)
);
create unique index ix_url_user on favourite(url,id_user);

create table password_recovery (
    id bigint identity primary key,
    token varchar(100) unique not null,
    id_user bigint not null,
    created_on datetime not null,
    constraint fk_password_recovery_user foreign key(id_user) references user(id)
);


/*
 * Test data.
 *
 * Password hashed using org.springframework.security.crypto.password.StandardPasswordEncoder.
 * Test users password is always "test".
 */
insert into user (id, username, password, enabled, member_since) values (1, 'leonardo.deseta@ideasagiles.com', '1af745bdd734e82a62f8042f6ed27715ced6f81ab9a561b7675e5ed89744096d56edbaf89010cc56', true, (current_date - 1 year));
insert into user (id, username, password, enabled, member_since) values (2, 'zim@ideasagiles.com', '1af745bdd734e82a62f8042f6ed27715ced6f81ab9a561b7675e5ed89744096d56edbaf89010cc56', true, (current_date - 1 year));
insert into user (id, username, password, enabled, member_since) values (3, 'admin@ideasagiles.com', '1af745bdd734e82a62f8042f6ed27715ced6f81ab9a561b7675e5ed89744096d56edbaf89010cc56', true, (current_date - 1 year));

insert into authority (id_user, authority) values (1, 'user');
insert into authority (id_user, authority) values (2, 'user');
insert into authority (id_user, authority) values (3, 'user');
insert into authority (id_user, authority) values (3, 'admin');

insert into feed (name, url, channel, created_on, id_user) values ('Dos Ideas', 'http://www.dosideas.com/noticias.feed?type=rss', 'software development', (current_date - 1 year), 1);
insert into feed (name, url, channel, created_on, id_user) values ('IGN Videogames', 'http://feeds.ign.com/ign/games-articles?format=xml', 'games', (current_date - 2 year), 1);
insert into feed (name, url, channel, created_on, id_user) values ('IGN PC Games', 'http://feeds.ign.com/ign/pc-all?format=xml', 'games', (current_date - 6 month), 1);

insert into feed_pack (pack, channel, name, url) values ('art', 'Art & Photos', 'Daily Imagery', 'http://wvs.topleftpixel.com/index_fullfeed.rdf');
insert into feed_pack (pack, channel, name, url) values ('art', 'Art & Photos', 'Photo Dojo', 'http://www.photojojo.com/content/feed');
insert into feed_pack (pack, channel, name, url) values ('comedy', 'comedy', 'Dilbert', 'http://feeds.feedburner.com/DilbertDailyStrip');
insert into feed_pack (pack, channel, name, url) values ('comedy', 'comedy', 'Wumo', 'http://feeds.feedburner.com/wulffmorgenthaler');
