# Uno Reader
##### A web-based newsfeed reader.

## What is Uno Reader?
Uno Reader is a web-based newsfeed reader. It is simple, elegant, and fast.
Uno Reader lets you read the latests news from your favourite websites, in one place.
Its UI uses responsive design, so you can access Uno Reader using your desktop computer,
your tablet or smartphone.

You can enjoy Uno Reader at [www.unoreader.com](http://www.unoreader.com)

## License
Uno Reader is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at Mozilla Public License FAQ.